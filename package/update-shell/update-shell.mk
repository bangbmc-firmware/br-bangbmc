
UPDATE_SHELL_VERSION = 84f30f1794df18e6b7d400fd419ddd18530e3f34
UPDATE_SHELL_SITE = $(BR2_BANGBMC_ROOT_URL)/update-shell.git
UPDATE_SHELL_SITE_METHOD = git
UPDATE_SHELL_INSTALL_TARGET = YES
UPDATE_SHELL_AUTORECONF = YES
UPDATE_SHELL_DEPENDENCIES = host-pkgconf


# Give the shell user a fix uid/gid.
# The reason for this is that buildroot does/has changed their
# uid/gid mapins so auto-assign may drift. this normally would not be
# a problem, however dev-data has uid and gids embedded in it, which means an
# update would break dev-data. This has already happened, so this change should
# prevent further breakage (or at least generated a build time error instead
# of silently breaking things).
define UPDATE_SHELL_USERS
	update 768 update 768 * /usr/libexec/update /usr/bin/update-shell - user account for performing system updates
endef

# update-su is only executable 
# by root(but it will refuse to run) and by members
# of the update group.
define UPDATE_SHELL_PERMISSIONS
	/usr/bin/update-su f 4750 0 update - - - - -
endef

define UPDATE_SHELL_ADD_MKSH_TO_SHELLS
	echo "/usr/bin/update-shell" >> $(TARGET_DIR)/etc/shells
endef

UPDATE_SHELL_TARGET_FINALIZE_HOOKS += UPDATE_SHELL_ADD_MKSH_TO_SHELLS

$(eval $(autotools-package))
