
OP_FAN_DAEMON_VERSION = dfeeb1a89426961dd338c29167a3ea1f3344637c
OP_FAN_DAEMON_SITE = $(BR2_BANGBMC_ROOT_URL)/op-fan-daemon
OP_FAN_DAEMON_SITE_METHOD = git
OP_FAN_DAEMON_INSTALL_TARGET = YES
OP_FAN_DAEMON_AUTORECONF = YES
OP_FAN_DAEMON_AUTORECONF_OPTS = -i -f -I $(HOST_DIR)/share/autoconf-archive
OP_FAN_DAEMON_DEPENDENCIES = host-pkgconf host-autoconf-archive \
										   hiomapd ipmi-bt-miniroute

$(eval $(autotools-package))
