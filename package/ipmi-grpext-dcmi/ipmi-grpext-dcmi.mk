
IPMI_GRPEXT_DCMI_VERSION = 7cc759bbc680ac845d7278a08f347a359c7fd61a
IPMI_GRPEXT_DCMI_SITE = $(BR2_BANGBMC_ROOT_URL)/ipmi-grpext-dcmi
IPMI_GRPEXT_DCMI_SITE_METHOD = git
IPMI_GRPEXT_DCMI_INSTALL_TARGET = YES
IPMI_GRPEXT_DCMI_AUTORECONF = YES
IPMI_GRPEXT_DCMI_AUTORECONF_OPTS = -i -f -I $(HOST_DIR)/share/autoconf-archive
IPMI_GRPEXT_DCMI_DEPENDENCIES = host-pkgconf host-autoconf-archive \
										   ipmi-bt-miniroute

$(eval $(autotools-package))
