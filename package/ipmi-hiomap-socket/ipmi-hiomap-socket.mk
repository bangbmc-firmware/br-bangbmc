
IPMI_HIOMAP_SOCKET_VERSION = 84645cc3c1810d98cb6f1ef79ff1d94a96a40ab5
IPMI_HIOMAP_SOCKET_SITE = $(BR2_BANGBMC_ROOT_URL)/ipmi-hiomap-socket
IPMI_HIOMAP_SOCKET_SITE_METHOD = git
IPMI_HIOMAP_SOCKET_INSTALL_TARGET = YES
IPMI_HIOMAP_SOCKET_AUTORECONF = YES
IPMI_HIOMAP_SOCKET_AUTORECONF_OPTS = -i -f -I $(HOST_DIR)/share/autoconf-archive
IPMI_HIOMAP_SOCKET_DEPENDENCIES = host-pkgconf host-autoconf-archive \
										   ipmi-bt-miniroute

$(eval $(autotools-package))
