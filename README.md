# br-blackbird-external

External buildroot tree. For building minimal Raptor Computer Systems Blackbird or Talos II bmc images

requirements:
* u-boot tools' mkimage (with FIT support)

Recommend/Tested buildroot version: 2022.02.10

To build the blackbird image, you just be in the root of buildroot, and run:
```
make BR2_EXTERNAL=${PATH_TO_THIS_BR_EXTERNAL} O=${BUILD_DIR} blackbird-bmc_defconfig
make O=${BUILD_DIR}
```

The Talos II directions are similar:
```
make BR2_EXTERNAL=${PATH_TO_THIS_BR_EXTERNAL} O=${BUILD_DIR} talos-bmc_defconfig
make O=${BUILD_DIR}
```
