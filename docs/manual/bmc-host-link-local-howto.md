# Using the usb ethernet interface for !BMC updates

Updating firmware via the ecm gadget's IPv6 address of bangBMC is simple.
The following example assume that 'usb0' is the name of the host's ecm side of
the gadet interface.

1)  First you need to discover your ipv6ll address
run 'ip addr show dev usb0'
```
ip addr show dev usb0
8: usb0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 5e:2f:07:9c:7e:58 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::5c2f:7ff:fe9c:7e58/64 scope link
       valid_lft forever preferred_lft forever
```
from this we can see the the host's address is 'fe80::5c2f:7ff:fe9c:7e58'


2)  Next discover the BMC's address. To do that run a broadcast ping on the gadget device.

```
ping6 -c 2 -I usb0 ff02::1
64 bytes from fe80::5c2f:7ff:fe9c:7e58%usb0: icmp_seq=1 ttl=64 time=0.050 ms
64 bytes from fe80::54d9:9dff:fe81:b3fa%usb0: icmp_seq=1 ttl=64 time=1.06 ms (DUP!)
64 bytes from fe80::5c2f:7ff:fe9c:7e58%usb0: icmp_seq=2 ttl=64 time=0.025 ms
```
from this we can clearly see the BMC (which is the non-host entry) is fe80::54d9:9dff:fe81:b3fa


3)  Finally send the update:
```
scp /tmp/update.tar update@\[fe80::54d9:9dff:fe81:b3fa%usb0\]:
```
