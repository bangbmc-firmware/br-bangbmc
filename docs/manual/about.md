## About !BMC

!BMC  (or bangBMC for searches, file paths, etc) is a minimal OS mean provide only the needed software to boot the host machine, and an ssh server. The name is intended to convery the fact that !BMC's primary goal is ***not*** be a full blown BMC stack.

!BMC's secondary goal is to provide a readable example of how to boot the host. Other BMC software can be a tangled web of service dependancies, and the source code may abuse features of languages like C++ so much that it is very difficult to understand. Any code specific to !BMC is intended to be written largely in C and shell scripts, with comments explaining non-obvious sections.

Currently !BMC only supports the Talos II and Blackbird motherboards from Raptorcs (!BMC is not affiliated with Raptorcs).

## Recommended knowledge

When modifying !BMC knowledge of [buildroot](http://buildroot.org) is very important. !BMC is implemented as a buildroot [external tree](https://buildroot.org/downloads/manual/manual.html#outside-br-custom).

## !BMC partition layout

The partition layout for currently support !BMC boards are (at this time) all the same.
* u-boot         384KiB (same as OpenBMC)
* u-boot-env     128KiB (same as OpenBMC)
* kernel-a       6MiB   (Longer than OpenBMC)
* dev-data       1MiB
* kernel-b       6MiB
* rwfs           18.50MiB

The first two partition are the same as OpenBMC (if someone is migrating off OpenBMC !BMC does not require changes. However any security patches will still be relevent). !BMC's kernel-a partion starts at the same offset as OpenBMC's, however it is longer.
!BMC has a small 1MiB 'dev-data' squashfs partition for storing things like an RFD root password, device serial numbers, or any other information that should persist even in the even the rwfs is wiped. A valid dev-data partition is not required to boot if !BMC is unable to mount the squashfs it will simply skip it. However the lack of dev-data may result in undesirable security situations.
!BMC also has a second kernel patition, kernel-b. Systems can choose which to boot via u-boot env scripts. !BMC does not itself include u-boot, however an example is in the works.
The rwfs is a read/writeable jffs2 partition for persistent storage. It is also not required to boot.

At startup, the init script in the kernel initrd (embedded in the u-boot FIT image which lives in the 'kernel-a' or 'kernel-b' partiton) will create an overlay like 'initrd,dev-data,rwfs' with the initrd being the bottom layer, and the rwfs being the top/modifiable layer.
In the event the rwfs is not present, a portion of the initrd will be substitued for the top lay so that the rootfs remains read/writeable.
The dev-data partition will be skip if the script is unable to mount it.
If neither the dev-data nor the rwfs can be mounted the script will forgo the overlay and just use the initrd image.

## Build instructions

building !BMC requires a buildroot repo (see the README.md for details).

!BMC can be built with:
```
make BR2_EXTERNAL=${PATH_TO_THIS_BR_EXTERNAL} O=${BUILD_DIR} blackbird-bmc_defconfig
make O=${BUILD_DIR}
```
For Talos II builds, replace blackbird-bmc_defconfig with talos-bmc_defconfig.

A 'dev-data' image, with a randomly generated root password, can be generated with:
```
${PATH_TO_THIS_BR_EXTERNAL}/board/bangBMC/gen-dev-squash --buildroot-base ${BUILD_DIR}
```
The output squashfs will be placed in ${BUILD_DIR}/images/dev-squash

## build output/usage

The primary output of the buildroot + external tree is
```
${BUILD_DIR}/images/uboot-fit.img
```
which is (as the name suggests) a u-boot FIT image. This image can be booted with tftp, or it maybe flashed onto the kernel partition, and booted with u-boot (again no actual changes to u-boot are required). 

## Errata

Known issues/limitations for !BMC:
* Current musl affected by 2038. The current version of musl has a signed, 32-bit time_t ABI. Musl devs say this will be fixed in the next release or the release after. No !BMC code changes are required, simply rebuilding with a buildroot that uses a fixed version of musl will resolve the issue.
* Corrupt/non-present dev-data will allow root login with build password. Ideally if dev-data is not present root login would be disabled. However that would break the tftpboot use case.
