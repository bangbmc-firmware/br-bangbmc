## Update mechansim design goals
The goals for the this update mechanism are to have no dedicated persistent daemon running on the BMC, while still maintaining an easy and flexible update mechasim.

## How it works
On the BMC is a user (currently named 'update'). This user has two special settings. First it has a gpg keyring containing one or more public keys authorized to validate updates. Second it's shell is set to the 'update-shell' program.

Update are performed by simply scp-ing a update file to the update user on the BMC.

### update file format.
The update is a nested tarball.
The outer tar ball must be uncompressed and contain exactly two files:
```
update.tar.xz
update.tar.xz.sig
```
update.tar.xz.sig must be a valid gpg signature of update.tar.xz with one of the authorized keys.

The exact format of update.tar.xz is not defined except that it must contain an executable named 'update-install' in the root of the archive (it is anticipated that this will be a shell script that will perform most of the install).

### update-su
update-su is a suid binary that accepts one argument, a path to temporary directory. Assuming the temporary directory matches the pattern that update-shell uses, the update-install in that directory launched as root.

### update-shell
The update user's shell, 'update-shell', is a simple program that examins the commandline arguments and errors out with a message in all cases except where it's detected that sshd/a remote scp binary (or a program that uses the same calling convention) has invoked it to recieve a file.

There upon it creates a temporary working directory, and invokes a child scp process to recieve the file with a fixed name in that directory.

After recieving the update, the tar archive is extracted, and the signature verified ***before*** the inner archive (update.tar.xz) is extracted. If the signature matches, and no other errors have occured, update-su is invoked with the update temp directory (which will run the update's update-install as root).
