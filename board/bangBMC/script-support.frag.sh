# This is a shell fragment, it makes no sense to excute directly
# It setups the arguments and env for the other buildroot scripts

# I've not looked into it much, but it seems without the
# empty short opt args (-o "") getopt does some weird stuff that breaks
# arg parsing.
TEMP=$(getopt -o "" --long 'board:' -n "$0" -- "$@")
if [ $? -ne 0 ] ; then
    echo "Error parsing arguments"
    exit 1
fi

eval set -- "${TEMP}"
unset TEMP

while true; do
    case "$1" in
        '--board')
            BOARD="$2"
            shift 2;
            ;;
        --)
            break
            ;;
        *)
            echo "Invalid argument $1"
            exit 1;
            ;;
    esac
done
