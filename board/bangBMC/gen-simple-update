#!/bin/sh
set -e

TEMP=$(getopt -o "" --long \
    'buildroot-base:,external:,keyring:,output-name:,help,debug' -n "$0" -- "$@")
if [ $? -ne 0 ] ; then
    echo "Error parsing arguments"
    exit 1
fi

eval set -- "${TEMP}"
unset TEMP

usage() {
    echo -e "\
usage $0 --buildroot-base \${buildroot-O-path} --output-name /tmp/dev-sqfs \
--addition-tree \${addon_tree}
    --buildroot-base    (required) Buildroot output directory. Same as the value given for
                        make O=\${some-dir} when making the buildroot.
    --output-name       (optional) path and file name to give the output update image.
                        Defaults to \${buildroot-base}/images/update.tar
    --external          (required) Base of the external buildroot tree.
    --keyring           (optional) gpg keyring to use.
                        (WARNING: will default to dev keyring)
    --help              Prints this message
    --debug             runs 'set -x' in the script.
"
    exit 0;
}

# do not resolve the base_dir just yet.
# Setting this, without resolving it allows optionally change output_name
output_name='${base_dir}/images/update.tar'
keyring='${external_dir}/board/bangBMC/bangBMC-signingring.gpg'
while true; do
    case "$1" in
        '--buildroot-base')
            base_dir="$2"
            shift 2;
            ;;
        '--output-name')
            output_name="$2"
            shift 2;
            ;;
        '--external')
            external_dir="$2"
            shift 2;
            ;;
        '--keyring')
            keyring="$2"
            shift 2;
            ;;
        '--help')
            usage
            break
            ;;
        '--debug')
            set -x
            shift
            ;;
        --)
            break
            ;;
        *)
            echo "Invalid argument $1"
            exit 1;
            ;;
    esac
done

# This causes output_name to be expanded.
# If it's not supplied then it will be relative to the supplied
# base_dir.
eval output_name="${output_name}"
eval keyring="${keyring}"

if [ -z "${base_dir}" ] ; then
    echo "Error: buildroot-base not specified" >&2
    exit 1
fi

if [ -z "${external_dir}" ] ; then
    echo "Error: external tree not specified" >&2
    exit 1
fi

if [ ! -d ${base_dir}/images ] ; then
    echo "Error image directory in \"${base_dir}\" does not exist." >&2
    exit 1
fi

work_dir=$(mktemp -d)
cp ${base_dir}/images/uboot-fit.img ${work_dir}/kernel.fit
cp ${external_dir}/board/bangBMC/update-install ${work_dir}
tar -cJf ${work_dir}/update.tar.xz -C ${work_dir} kernel.fit update-install
gpg --no-default-keyring --keyring=${keyring} -b ${work_dir}/update.tar.xz
tar -cf ${work_dir}/update.tar -C ${work_dir} update.tar.xz update.tar.xz.sig
cp ${work_dir}/update.tar ${output_name}

rm -rf ${work_dir}
