#!/bin/sh
#
# Copyright 2018 - 2019 Raptor Engineering, LLC
# Released under the terms of the GPL v3

I2C_BUS=12
I2C_DEV=0x31

# ***** FRONT PANEL LEDS *****
# Set HDD LED inverted
i2cset -y ${I2C_BUS} ${I2C_DEV} 0x11 0x1
