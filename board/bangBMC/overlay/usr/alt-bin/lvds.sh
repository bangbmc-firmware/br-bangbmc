#!/bin/sh
#
# Copyright 2018 - 2019 Raptor Engineering, LLC
# Released under the terms of the GPL v3

# raptor blackbird bmc's have a device tree entry identifying themselves
# as such, let us check it, for sanity reasons.
# Also the Talos II doesn't need this
board_type=$(cat /proc/device-tree/compatible  |tr '\0' '\n' | head -n 1)

if [ ! "${board_type}" = "rcs,blackbird-bmc" ] ; then
    echo "It seems this is not a raptorcs/blackbird bmc, exiting"
    exit 0
fi

# ***** BMC PINMUX *****

# HACK
# Work around pinmux hog failure on cold boot
# Remove this block once the pinmux is fixed in the kernel!
devmem 0x1e6e2094 32 0x00004001
devmem 0x1e6e208c 32 0x000000ff
devmem 0x1e6e20a4 32 0xfffff000
devmem 0x1e6e20a8 32 0x000fffff
devmem 0x1e6e6064 32 0x00000080


# ***** SET UP ITE HDMI TRANSCEIVER *****

I2C_BUS=5
I2C_DEV=0x4c

# ***** GENERAL *****
# Reset device
i2cset -y ${I2C_BUS} ${I2C_DEV} 0x4 0x1c


# ***** VIDEO *****
# Set bank 0
i2cset -y ${I2C_BUS} ${I2C_DEV} 0x0f 0x08	# 0x0f[1:0] = 0

# AVMute output
i2cset -y ${I2C_BUS} ${I2C_DEV} 0xc1 0x01	# 0xc1[0] = 1

# Take device out of reset
i2cset -y ${I2C_BUS} ${I2C_DEV} 0x04 0x00

# Enable HDMI transmitter reset
i2cset -y ${I2C_BUS} ${I2C_DEV} 0x61 0x10

# Configure input signal
i2cset -y ${I2C_BUS} ${I2C_DEV} 0x70 0x00	# RGB mode, I/O latch at TxClk, non-CCIR656, non-embedded sync, single edge, no PCLK delay
i2cset -y ${I2C_BUS} ${I2C_DEV} 0x90 0x00	# PG horizontal total = 0, H/V sync provided by external driver, active low VSYNC, active low HSYNC, Data Enable provided by external driver

# Enable DVI mode (works for HDMI as well, host should configure HDMI later in the boot process)
i2cset -y ${I2C_BUS} ${I2C_DEV} 0xc0 0x00	# 0xc0[0] = 0

# Release HDMI transmitter reset
i2cset -y ${I2C_BUS} ${I2C_DEV} 0x61 0x00	# 0x61[4] = 0

# Un-AVMute output
i2cset -y ${I2C_BUS} ${I2C_DEV} 0xc1 0x00	# 0xc1[0] = 0

# ***** AUDIO *****
# Disable audio channel
i2cset -y ${I2C_BUS} ${I2C_DEV} 0xe0 0x08	# 0xe0[3:0] = 0
