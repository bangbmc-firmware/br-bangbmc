#!/bin/sh
set -e
TARGET_DIR="$1"

TOPDIR=$(pwd)
IMAGES_DIR=${BASE_DIR}/images
BOARD_DIR=${BR2_EXTERNAL_bangBMC_PATH}/board/bangBMC
# the first argument is always a script specific buildroot dir
shift 1
. ${BOARD_DIR}/script-support.frag.sh

[ -z "${TARGET_DIR}" ] && echo "empty target dir" && exit 1

echo "disabling undesirable init scripts"

# TODO eventually we want to actually use the hwclock,
# so we should probably fix it instead of removing it.
blacklisted_init_scripts='netmount
hwclock
swap
keymaps
numlock
binfmt
termencoding
consolefont
save-keymaps
save-termencoding
net-online
staticroute
'

for drop in $(find ${TARGET_DIR}/etc/init.d ${TARGET_DIR}/etc/runlevels) ; do
    bn=$(basename ${drop})
    # if it's not in out list, ignore it
    [ -z "$(echo ${blacklisted_init_scripts} | grep ${bn})" ] && continue
    rm -v ${drop}
done

blacklisted_gpgp_files="gpgrt-config \
gpg-error \
gpg \
gpgsm \
gpg-agent \
gpgconf \
gpg-connect-agent \
gpgparsemail \
gpg-wks-server \
gpgtar \
gpgscm \
gpg-error \
gpg-error.asd \
gpg-error-package.lisp \
gpg-error.lisp \
gpg-error-codes.lisp \
gpg-protect-tool \
gpg-preset-passphrase \
gpg-wks-client \
libksba.so* \
libnpth.so* \
libassuan.so* \
dirmngr* \
\
ksba-config \
npth-config \
addgnupghome \
applygnupgdefaults \
"

for nongpg in ${blacklisted_gpgp_files} ; do
    rm -rf $(find ${TARGET_DIR} -name ${nongpg})
done
