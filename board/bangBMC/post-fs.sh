#!/bin/sh
set -e

TOPDIR=$(pwd)
IMAGES_DIR=${BASE_DIR}/images
BOARD_DIR=${BR2_EXTERNAL_bangBMC_PATH}/board/bangBMC
# the first argument is always a script specific buildroot dir
shift 1;
. ${BOARD_DIR}/script-support.frag.sh


dtc -I dts -O dtb -o ${IMAGES_DIR}/${BOARD}.dtb ${BOARD_DIR}/${BOARD}.dts
cp ${BOARD_DIR}/${BOARD}.its ${IMAGES_DIR}
DTS_OPTS="-I dts -O dtb -p 2000"

ORI_DIR=$(pwd)
cd ${IMAGES_DIR}

mkimage -D "${DTS_OPTS}" -f ${IMAGES_DIR}/${BOARD}.its uboot-fit.img
# false
